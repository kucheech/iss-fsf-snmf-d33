### bower init
```bower init```

### install ionic v1
```bower install ionic-v1 --save```


http://ionicframework.com/docs/v1/components/


### use angular v1
```
(function() {
    'use strict';

    angular.module('MyApp', ["ionic"]);

})();
```
